Notice
======

MyPassword
------
Ce programme permet de sauvegarder vos mots de passe dans une banque de donnée chiffrée.
Pour cela, il utilise les algorithmes de cryxor (masque jetable) directement sur le fichier sans que jamais aucune informations en claire ne soit sauvegardée. (tout est dans la RAM).

Il vous est recommandé très fortement d'utiliser un mot de passe long, si possible semi-aléatoire utilisant des caractères spéciaux.
La phrase à l'affichage a deux utilitées :
- Permetre de proteger le mot de passe (en effet, si elle n'existait pas, il serait facile de trouver une partie du mot de passe, les 6 premiers caractère, par une simple opération mathématique.
- Vous permetre de vérifier que vous ne vous êtes pas trompé de mot de passe. Si vous validez alors que le mot de passe n'est pas correct, il peut y avoir perte de données par ecrasement (en cas de sauvegarde uniquement).

Cryxor2013
------
Ce programme permet de chiffrer/déchiffrer des données en suivant l'algorithme de cryxor (masque jetable).
Il est conseillé d'utiliser un mot de passe totalement aléatoire, c'est à dire utiliser la fonction de génération de fichier clef aléatoire.
L'utilisation de passphrase est déconseillée mais peut être sécurisée dans le cas de petits fichiers.

IMPORTANT :
La sécurité n'est "totale" avec ce logiciel que si vous suivez scrupuleusement la consigne suivante :
UNE CLEF ALEATOIRE c'est à dire :
- unique (ne pas la réutiliser pour une autre donnée),
- générée aléatoirement (sinon il est possible mathématiquement de la retrouver... avec beaucoup d'efforts !!!)
- de la même taille que le fichier à chiffrer (sinon on peut trouver des motifs cassables comme si la clef n'était pas unique)

Cependant, les moyens necessaires pour craquer la sécurité d'un fichier chiffré de cette manière, dans le cas où la clef est suffisament grande et aléatoire sont trop important pour un organisme simple. Cette sécuritée est suffisante pour des données confidentielles classiques.
Ne pas utiliser dans un contexte majeur (terrorisme, données militaires,...)
