/*
CRYXOR 2013 EST SOUS LICENCE GNU GPL
Programme de chiffrement et de dechiffrement symetrique -Masque Jetable-
Sopheny (Cassiopeeu) 2013
Le programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifi
er au titre des clauses de la Licence Publique Générale GNU, telle que publiée p
ar la Free Software Foundation ; soit la version 2 de la Licence, ou (à votre di
scrétion) une version ultérieure quelconque. Ce programme est distribué dans l'e
spoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie impli
cite de COMMERCIABILITE ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la
icence Publique Générale GNU pour plus de détails. Vous devriez avoir reçu un ex
emplaire de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas
le cas, écrivez à la Free Software Foundation Inc.,  51 Franklin Street, Fifth F
loor, Boston, MA 02110-1301, USA.
*/

#include "mypassword_windows.hpp"
#include <iostream>

MyPassword_Windows::MyPassword_Windows() : QWidget()
{
    initializeAllWidgets();
    initializeAllLayouts();
    initializeAllSignals();
}

int MyPassword_Windows::initialize()
{
    // créer le répertoire
    QDir dir;
    dir.mkdir(QString(QDir::homePath() + "/.cryxor2013/"));

    m_password = QInputDialog::getText(this,tr("Mot de Passe"),
                 tr("Saisissez votre mot de passe pour débloquer l'accès à MyPassword. Attention, il est impossible de le changer.\nDe préférence, choississez un mot de passe long, comme une phrase entière par exemple. Ce mot de passe devra être mémorisé.\n\nVous n'êtes pas obligé de faire cette opération tout de suite, appuyez sur annuler pour passer et aller à Cryxor."), QLineEdit::Password);

    //on bloque si le mot de passe est vide (cancel)
    if (m_password.isEmpty())
        return (1);
    else
    {
        m_passwords_firstline = QInputDialog::getText(this,tr("Validation"),tr("Saisissez une phrase qui s'affichera lors du débloquage le valider. Utilisez la phrase la plus longue possible, voir utilisez des signes aléatoire et des chiffres si vous le souhaitez.\nVous n'avez pas besoin de mémoriser cette information.\nElle vous sera affichée lors du déblocage prochain de vos mots de passe, afin de vous permettre de vérifier vous-même si vous avez bel est bien entré votre mot de passe.\nIl n'y a aucune procédure automatique permettant de vérifier cela.")) + "\n";
        // On imprime la première ligne chiffrée
        m_cryxor.setkfile(m_password);
        m_cryxor.setifile(m_passwords_firstline);
        m_cryxor.setofile(m_passwordpath);
        m_cryxor.setmode(4);
        m_cryxor.encryptQ();
        return (0);
    }
}

void MyPassword_Windows::initializeAllWidgets()
{
    this->hide();
    // MAIN WIDGET
    m_layoutMain = new QVBoxLayout(this);

    // MAIN
    m_layoutLock = new QVBoxLayout(this);
    m_layoutContent_view = new QVBoxLayout(this);
    m_layoutContent_buttons = new QHBoxLayout(this);

    m_buttonLock = new QPushButton(this);
    m_buttonQuit = new QPushButton(this);
    m_buttonReinit = new QPushButton(this);

    m_buttonSavePassword = new QPushButton(this);
    m_buttonAddPassword = new QPushButton(this);
    m_buttonDelPassword = new QPushButton(this);
    m_buttonChgPassword = new QPushButton(this);
    m_buttonDspPassword = new QPushButton(this);

    m_passwordslist_view = new QListView(this);
    m_passwordslist_model = new QStringListModel(this);
    m_passwordslist_view->setModel(m_passwordslist_model);
    m_passwordslist_view->setEditTriggers(QAbstractItemView::NoEditTriggers);

    m_locked = false;
    m_buttonLock->setText("Initialize MyPassword");
    m_passwordpath = QDir::homePath() + QString::fromUtf8(MY_PASSWORD_PATH);
    manage_lock(); // manage lock remet locked sur true et initialize les bonnes valeurs

    //m_passwordslist_view->setMovement(QListView::Static);
    m_passwordslist_view->setSelectionMode(QAbstractItemView::SingleSelection);
    m_buttonQuit->setText(tr("Quit"));
    m_buttonReinit->setText(tr("Reinitialize MyPassword"));
    m_buttonSavePassword->setText(tr("Save"));
    m_buttonAddPassword->setText(tr("Add"));
    m_buttonDelPassword->setText(tr("Del"));
    m_buttonChgPassword->setText(tr("Change"));
    m_buttonDspPassword->setText(tr("Display"));
    return;
}

void MyPassword_Windows::initializeAllLayouts()
{
    m_layoutMain->addLayout(m_layoutContent_view);
    m_layoutMain->addLayout(m_layoutContent_buttons);
    m_layoutMain->addLayout(m_layoutLock);
    m_layoutLock->addWidget(m_buttonLock);
    m_layoutLock->addWidget(m_buttonReinit);
    m_layoutLock->addWidget(m_buttonQuit);
    m_layoutContent_view->addWidget(m_passwordslist_view);
    m_layoutContent_buttons->addWidget(m_buttonAddPassword);
    m_layoutContent_buttons->addWidget(m_buttonChgPassword);
    m_layoutContent_buttons->addWidget(m_buttonDspPassword);
    m_layoutContent_buttons->addWidget(m_buttonDelPassword);

    m_layoutContent_view->addWidget(m_buttonSavePassword);
    return;
}

void MyPassword_Windows::initializeAllSignals()
{
    connect(m_buttonLock, SIGNAL(clicked()), this, SLOT(manage_lock()));
    connect(m_buttonQuit, SIGNAL(clicked()), this, SLOT(quit()));
    connect(m_buttonReinit, SIGNAL(clicked()), this, SLOT(reinit_password()));
    connect(m_buttonSavePassword, SIGNAL(clicked()), this, SLOT(save_password()));
    connect(m_buttonAddPassword, SIGNAL(clicked()), this, SLOT(add_password()));
    connect(m_buttonDelPassword, SIGNAL(clicked()), this, SLOT(del_password()));
    connect(m_buttonChgPassword, SIGNAL(clicked()), this, SLOT(chg_password()));
    connect(m_buttonDspPassword, SIGNAL(clicked()), this, SLOT(dsp_password()));
    connect(m_passwordslist_view, SIGNAL(clicked(QModelIndex)), SLOT(check_password_buttons(QModelIndex)));
    return;
}

int MyPassword_Windows::manage_lock(bool popup)
{
    // Tester la première utilisation
    if (popup && !QFileInfo(m_passwordpath).isDir() && !QFile(m_passwordpath).exists())
        initialize();

    if (m_locked == true)
    {
        get_password();
        if (m_password.isEmpty())
            return (0);

        m_cryxor.setkfile(m_password);
        m_cryxor.setifile(m_passwordpath);
        m_cryxor.setmode(3);
        m_cryxor.encryptQ();

        if (valid_password())
        {
            m_buttonSavePassword->setEnabled(true);
            m_buttonAddPassword->setEnabled(true);
            m_buttonLock->setText(tr("&Lock"));
            m_buttonReinit->setEnabled(true);
            m_locked = false;

            //on conserve la ligne de vérification de coté pour
            //ne pas l'afficher dans la liste
            m_passwordslist = m_cryxor.getofile().split("\n");
            m_passwords_firstline = m_passwordslist.first();
            m_passwordslist.removeFirst();
        }
    }
    else
    {
        if (m_passwordslist.size() > 0)
        if (popup && QMessageBox::question(this, tr("Confirmer"), tr("Voulez-vous enregister les dernières modifications effectuées ?"),
                                  QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
            save_password();
        m_password = "";
        m_locked = true;
        m_buttonLock->setText(tr("&Unlock"));
        m_buttonReinit->setEnabled(false);
        m_buttonSavePassword->setEnabled(false);
        m_buttonAddPassword->setEnabled(false);
        m_buttonDelPassword->setEnabled(false);
        m_buttonChgPassword->setEnabled(false);
        m_buttonDspPassword->setEnabled(false);
        m_passwordslist.clear();
    }
    update_passwordslist_control();
    return 0;
}

void MyPassword_Windows::get_password()
{
    m_password = QInputDialog::getText(this,tr("Mot de Passe"),tr("Saisissez votre mot de passe pour débloquer l'accès à MyPassword"),
                                       QLineEdit::Password);
    return;
}

bool MyPassword_Windows::valid_password()
{
    if (QMessageBox::question(this, tr("Confirmer"), m_cryxor.getofile().split("\n").first().toUtf8() + "\nEst-ce correct ?",
                              QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) == QMessageBox::Yes)
        return true;
    else
        return false;
    return false;
}

int MyPassword_Windows::write_entry(QString url, QString pseudo, QString email, QString password, QString q1, QString q2, QString q3)
{
    m_passwordslist.append(QString(url + "<space>" + pseudo + "<space>" + email + "<space>" + password + "<space>" + q1 + "<space>" + q2 + "<space>" + q3 + "\n"));
    update_passwordslist_control();
    return 0;
}

int MyPassword_Windows::remove_entry(quint64 id)
{
    m_passwordslist.removeAt(id);
    update_passwordslist_control();
    return 0;
}
void MyPassword_Windows::add_password()
{
    QString url      = QInputDialog::getText(this, tr("Nouvelle Entrée..."), tr("URL"));
    QString pseudo   = QInputDialog::getText(this, tr("Nouvelle Entrée") + " " + url, tr("Pseudo"));
    QString email    = QInputDialog::getText(this, tr("Nouvelle Entrée") + " " + url, tr("Email"));
    QString password = QInputDialog::getText(this, tr("Nouvelle Entrée") + " " + url, tr("Mot de Passe"), QLineEdit::Password);
    QString q1       = QInputDialog::getText(this, tr("Nouvelle Entrée") + " " + url, tr("Question secrete 1"));
    QString q2       = QInputDialog::getText(this, tr("Nouvelle Entrée") + " " + url, tr("Question secrete 2"));
    QString q3       = QInputDialog::getText(this, tr("Nouvelle Entrée") + " " + url, tr("Question secrete 3"));
    write_entry(url, pseudo, email, password, q1, q2, q3);
}
void MyPassword_Windows::del_password()
{
    remove_entry(m_passwordslist_view->currentIndex().row());
}
void MyPassword_Windows::chg_password()
{
    QList < QString> old_data;
    QList < QString> new_data;

    // on récupère les anciennes valeurs
    int id = m_passwordslist_view->currentIndex().row();
    for (int i=0;i<7;i++)
        old_data.append( m_passwordslist.at(id).split("<space>").at(i));

    // on demande les nouvelles
    new_data.append(QInputDialog::getText(this, tr("Edition de l'Entrée..."), tr("URL") + " " + old_data[0] + " (si correcte laisser vide)"));
    if (new_data[0].isEmpty()) new_data[0] = old_data[0];
    new_data.append(QInputDialog::getText(this, tr("Edition de l'Entrée") + " " + new_data[0], tr("Pseudo") + " " + old_data[1] + " (si correcte laisser vide)"));
    new_data.append(QInputDialog::getText(this, tr("Edition de l'Entrée") + " " + new_data[0], tr("Email") + " " + old_data[2] + " (si correcte laisser vide)"));
    new_data.append(QInputDialog::getText(this, tr("Edition de l'Entrée") + " " + new_data[0], tr("Mot de Passe") + " " + old_data[3] + " (si correcte laisser vide)", QLineEdit::Password));
    new_data.append(QInputDialog::getText(this, tr("Edition de l'Entrée") + " " + new_data[0], tr("Question secrete 1") + " " + old_data[4] + " (si correcte laisser vide)"));
    new_data.append(QInputDialog::getText(this, tr("Edition de l'Entrée") + " " + new_data[0], tr("Question secrete 2") + " " + old_data[5] + " (si correcte laisser vide)"));
    new_data.append(QInputDialog::getText(this, tr("Edition de l'Entrée") + " " + new_data[0], tr("Question secrete 3") + " " + old_data[6] + " (si correcte laisser vide)"));
    del_password();

    //on fusionne avec les anciennes
    for (int i=1; i<7;i++)
    {
        if (new_data[i].isEmpty())
            new_data[i] = old_data[i];
    }

    write_entry(new_data[0], new_data[1], new_data[2], new_data[3], new_data[4], new_data[5], new_data[6]);
}
void MyPassword_Windows::dsp_password()
{
    // TODO : Table , better display
    QStringList infoslist = m_passwordslist.at(m_passwordslist_view->currentIndex().row()).split("<space>");
    QString infos;
    infos += "URL : " + infoslist.at(0) + "\n";
    infos += "Pseudo : " + infoslist.at(1) + "\n";
    infos += "Email : " + infoslist.at(2) + "\n";
    infos += "Password : " + infoslist.at(3) + "\n";
    infos += "Question 1 : " + infoslist.at(4) + "\n";
    infos += "Question 2 : " + infoslist.at(5) + "\n";
    infos += "Question 3 : " + infoslist.at(6) + "\n";
    QMessageBox::information(this,tr("Informations"), infos);
}
int MyPassword_Windows::save_password()
{
    remove_old_password();
    m_cryxor.setifile(m_passwords_firstline + "\n" + m_passwordslist.join("\n"));
    m_cryxor.setkfile(m_password);
    m_cryxor.setofile(m_passwordpath);
    m_cryxor.setmode(4);
    m_cryxor.encryptQ();
    return 0;
}
void MyPassword_Windows::reinit_password()
{
    if (QMessageBox::question(this, tr("Confirmer"), tr("Êtes vous sûr de vouloir réinitialiser MyPassword ?"),
                              QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
    {
        remove_old_password();
    }
    manage_lock(false);
    return;
}

void MyPassword_Windows::update_passwordslist_control()
{
    m_passwordslist_control.clear();
    m_passwordslist.removeOne("");
    for (int i = 0; i < m_passwordslist.size(); i++)
    {
        m_passwordslist_control.append( \
                m_passwordslist.at(i).split("<space>").first());
    }
    m_passwordslist_model->setStringList(m_passwordslist_control);
}
void MyPassword_Windows::check_password_buttons(QModelIndex index)
{
    if (m_locked == false)
    {
        m_buttonSavePassword->setEnabled(true);
        m_buttonAddPassword->setEnabled(true);
        if (m_passwordslist_view->currentIndex().isValid())
        {
            m_buttonChgPassword->setEnabled(true);
            m_buttonDspPassword->setEnabled(true);
            m_buttonDelPassword->setEnabled(true);
        }
        else
        {
            m_buttonChgPassword->setEnabled(false);
            m_buttonDspPassword->setEnabled(false);
            m_buttonDelPassword->setEnabled(false);
        }
    }
    else
    {
        m_buttonSavePassword->setEnabled(false);
        m_buttonAddPassword->setEnabled(false);
        m_buttonDelPassword->setEnabled(false);
        m_buttonChgPassword->setEnabled(false);
        m_buttonDspPassword->setEnabled(false);
    }
}

void MyPassword_Windows::quit()
{
    if (m_locked == false)
        manage_lock();
    this->hide();
}

void MyPassword_Windows::remove_old_password()
{
    QFile remove_old_file_path(m_passwordpath);
    remove_old_file_path.remove();
    return;
}
