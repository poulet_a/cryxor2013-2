#ifndef CONFIGREADER_HPP
#define CONFIGREADER_HPP
#include <QCoreApplication>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QDataStream>
#include <fstream>

#define CONF_FILE_PATH QCoreApplication::applicationDirPath() + "/cryxor2013.conf"

/*
** DOCUMENTATION :
** DOCUMENTATION METHODES :
** static QString findParameter(QString)
**  Return to the value of the parameter in the cryxor2013.conf file
*/
class ConfigReader
{
    public:
        ConfigReader();
        static QString findParameter(QString parameter);
};

#endif // CONFIGREADER_HPP
