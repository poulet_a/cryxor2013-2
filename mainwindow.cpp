/*
CRYXOR 2013 EST SOUS LICENCE GNU GPL
Programme de chiffrement et de dechiffrement symetrique -Masque Jetable-
Sopheny (Cassiopeeu) 2013
Le programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifi
er au titre des clauses de la Licence Publique Générale GNU, telle que publiée p
ar la Free Software Foundation ; soit la version 2 de la Licence, ou (à votre di
scrétion) une version ultérieure quelconque. Ce programme est distribué dans l'e
spoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie impli
cite de COMMERCIABILITE ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la
icence Publique Générale GNU pour plus de détails. Vous devriez avoir reçu un ex
emplaire de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas
le cas, écrivez à la Free Software Foundation Inc.,  51 Franklin Street, Fifth F
loor, Boston, MA 02110-1301, USA.
*/

#include "mainwindow.hpp"
#include "configreader.hpp"

#include <iostream>

MainWindow::MainWindow(char *version) : QWidget()
{
    m_mypassword_windows = new MyPassword_Windows();
    // MENUS
    m_menuBar = new QMenuBar(this);
    m_menuFile = new QMenu(this);
    m_menuOption = new QMenu(this);
    m_menuAbout = new QMenu(this);
    m_menuFile_actionQuit = new QAction(this);
    m_menuOption_actionSafeRemoveFile = new QAction(this);
    m_menuOption_actionMyPassword = new QAction(this);
    m_menuAbout_actionGuidTXT = new QAction(this);
    m_menuAbout_actionGuidWeb = new QAction(this);
    m_menuAbout_actionAPropos = new QAction(this);
    // MAIN WIDGET
    m_layoutMain= new QVBoxLayout(this);
    // MAIN
    m_layoutFiles = new QGridLayout(this);
    m_buttonRun = new QPushButton(this);
    m_buttonQuit = new QPushButton(this);
    m_statusMainbar = new QStatusBar(this);
    // FILES
    m_labelIFile = new QLabel(this);
    m_labelKFile = new QLabel(this);
    m_labelOFile = new QLabel(this);

    m_labelIDeletion = new QLabel(this);
    m_boxIDeletion = new QCheckBox(this);

    m_lineIFile = new QLineEdit(this);
    m_lineKFile = new QLineEdit(this);
    m_lineOFile = new QLineEdit(this);

    m_buttonIFile = new QPushButton(this);
    m_buttonKFile = new QPushButton(this);
    m_buttonOFile = new QPushButton(this);
    m_buttonKMode = new QPushButton(this);
    m_buttonKCopy = new QPushButton(this);
    m_buttonKPaste = new QPushButton(this);
    m_buttonKGenerate = new QPushButton(this);

    initializeAllWidgets(QString(version));
    initializeAllLayouts();
    initializeAllSignals();
    return;
}

void MainWindow::initializeAllWidgets(QString version)
{
    //SETTING WIDGETS
    this->setWindowTitle("Cryxor2013 " + version);
    this->setLayout(m_layoutMain);
    this->setWindowIcon(QIcon(":/icones/images/icon_cryxor.png"));
    m_layoutMain->setMenuBar(m_menuBar);
    m_menuFile = m_menuBar->addMenu(tr("&Fichiers"));
    m_menuOption = m_menuBar->addMenu(tr("&Options"));
    m_menuAbout = m_menuBar->addMenu(tr("A propos"));
    m_menuFile_actionQuit = m_menuFile->addAction(tr("&Quitter"));
    m_menuOption_actionMyPassword = m_menuOption->addAction(tr("My Password"));
    m_menuOption_actionSafeRemoveFile = m_menuOption->addAction(tr("Supprimer un fichier de manière sécurisée"));
    m_menuAbout_actionGuidTXT = m_menuAbout->addAction(tr("Guide en TXT"));
    m_menuAbout_actionGuidWeb = m_menuAbout->addAction(tr("Guide en ligne"));
    m_menuAbout_actionAPropos = m_menuAbout->addAction(tr("A propos..."));

    m_buttonRun->setText(tr("Lancer le programme", "Lancer le Crypta/Decryta"));
    m_buttonQuit->setText(tr("Quitter", "Quitter le programme"));

    m_labelIFile->setText(tr("Fichier à charger","IFile label"));
    m_labelKFile->setText(tr("Votre clé","IFile label"));
    m_labelOFile->setText(tr("Emplacement de sauvegarde","IFile label"));

    m_labelIDeletion->setText(tr("Suppression du fichier après Chiffrement"));
    m_boxIDeletion->setChecked(false);
    m_boxIDeletion->setCheckable(true);

    m_lineIFile->setText(tr(""));
    m_lineOFile->setText(tr(""));
    m_lineKFile->setText(tr(""));

    m_buttonIFile->setText("...");
    m_buttonKFile->setText("...");
    m_buttonOFile->setText("...");

    m_buttonKCopy->setText(tr("Copier"));
    m_buttonKPaste->setText(tr("Coller"));
    m_buttonKMode->setText("");
    m_buttonKGenerate->setText(tr("Générer une clef aléatoire."));

    m_statusMainbar->showMessage("En Attente.");
    m_KMode = 0;

    execKMode();
    testBeforeEncrypt();
    return;
}

void MainWindow::initializeAllLayouts()
{
    // ADD WIDGET IN LAYOUT
    m_layoutMain->addLayout(m_layoutFiles);
    m_layoutMain->addWidget(m_buttonRun);
    m_layoutMain->addWidget(m_buttonQuit);
    m_layoutMain->addWidget(m_statusMainbar);

    m_layoutFiles->addWidget(m_labelIFile,0,0,1,1);
    m_layoutFiles->addWidget(m_labelIDeletion,1,0,1,2);
    m_layoutFiles->addWidget(m_boxIDeletion,1,2,1,1);
    m_layoutFiles->addWidget(m_labelKFile,2,0,1,1);
    m_layoutFiles->addWidget(m_labelOFile,4,0,1,1);
    m_layoutFiles->addWidget(m_lineIFile,0,1,1,3);
    m_layoutFiles->addWidget(m_lineKFile,2,1,1,3);
    m_layoutFiles->addWidget(m_lineOFile,4,1,1,3);
    m_layoutFiles->addWidget(m_buttonIFile,0,4,1,1);
    m_layoutFiles->addWidget(m_buttonKFile,2,4,1,1);
    m_layoutFiles->addWidget(m_buttonOFile,4,4,1,1);
    m_layoutFiles->addWidget(m_buttonKCopy,3,1,1,1);
    m_layoutFiles->addWidget(m_buttonKPaste,3,2,1,1);
    m_layoutFiles->addWidget(m_buttonKMode,3,3,1,1);
    m_layoutFiles->addWidget(m_buttonKGenerate,3,4,1,1);
    return;
}

void MainWindow::initializeAllSignals()
{
    connect(m_menuFile_actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(m_menuAbout_actionGuidTXT, SIGNAL(triggered()), this, SLOT(openGuidTXT()));
    connect(m_menuAbout_actionGuidWeb, SIGNAL(triggered()), this, SLOT(openGuidWeb()));
    connect(m_menuAbout_actionAPropos, SIGNAL(triggered()), this, SLOT(openAPropos()));
    connect(m_menuOption_actionMyPassword, SIGNAL(triggered()), m_mypassword_windows, SLOT(show()));
    connect(m_menuOption_actionSafeRemoveFile, SIGNAL(triggered()), this, SLOT(fileSafeRemove()));

    connect(m_buttonIFile, SIGNAL(clicked()), this, SLOT(selectIFile()));
    connect(m_buttonKFile, SIGNAL(clicked()), this, SLOT(selectKFile()));
    connect(m_buttonOFile, SIGNAL(clicked()), this, SLOT(selectOFile()));
    connect(m_buttonKMode, SIGNAL(clicked()), this, SLOT(switchKMode()));
    connect(m_buttonKCopy, SIGNAL(clicked()), this, SLOT(copyKLine()));
    connect(m_buttonKPaste, SIGNAL(clicked()), this, SLOT(pasteKLine()));
    connect(m_buttonKGenerate, SIGNAL(clicked()), this, SLOT(generateNewKey()));

    connect(m_lineIFile, SIGNAL(textChanged(QString)), this, SLOT(testBeforeEncrypt()));
    connect(m_lineKFile, SIGNAL(textChanged(QString)), this, SLOT(testBeforeEncrypt()));
    connect(m_lineOFile, SIGNAL(textChanged(QString)), this, SLOT(testBeforeEncrypt()));

    connect(m_buttonRun, SIGNAL(clicked()), this, SLOT(encrypt()));
    connect(m_buttonQuit, SIGNAL(clicked()), qApp, SLOT(quit()));
    return;
}

void MainWindow::execKMode()
{
    if (m_KMode == 0)
    {
        m_buttonKFile->setText("...");
        m_buttonKFile->setEnabled(true);
        m_buttonKCopy->setEnabled(false);
        m_buttonKPaste->setEnabled(false);
        m_buttonKMode->setText(tr("Changer de mode (>passphrase)", \
                                  "Changer le Kmode vers passphrase"));
        m_buttonKGenerate->setEnabled(true);
        m_lineKFile->setEchoMode(QLineEdit::Normal);
    }
    else if (m_KMode == 1)
    {
        m_buttonKFile->setEnabled(false);
        m_buttonKCopy->setEnabled(true);
        m_buttonKPaste->setEnabled(true);
        m_buttonKMode->setText(tr("Changer de mode (>key file)", \
                                  "Changer le Kmode vers keyFile"));
        m_buttonKGenerate->setEnabled(false);
        m_lineKFile->setEchoMode(QLineEdit::Password);
    }
    else
    {
        std::cout << "ERROR [GRAVE] : INVALID KMODE\n";
    }
    return;
}


void MainWindow::switchKMode()
{
    if (m_buttonKFile->isEnabled())
    {
        m_KMode = 1;
    }
    else
    {
        m_KMode = 0;
    }
    execKMode();
    testBeforeEncrypt();
    return;
}

void MainWindow::copyKLine()
{
    QClipboard *pp = QApplication::clipboard();
    pp->setText(m_lineKFile->text());
    return;
}

void MainWindow::pasteKLine()
{
    QClipboard *pp = QApplication::clipboard();
    m_lineKFile->setText(m_lineKFile->text() + pp->text());
    return;
}

void MainWindow::selectIFile()
{
    m_lineIFile->setText(\
                QFileDialog::getOpenFileName(this, \
                tr("Sélectionner fichier", \
                   "Selection du fichier à chiffrer"), \
                QString(), "All file (*)"));

    return;
}

void MainWindow::selectKFile()
{
    m_lineKFile->setText(
                QFileDialog::getOpenFileName(this, \
                tr("Sélectionner clé", \
                   "Selection du fichier contenant la clé"), \
                QString(), "All file (*)"));

    return;

}

void MainWindow::selectOFile()
{
    m_lineOFile->setText(saveFile());
}

QString MainWindow::saveFile()
{
    return QFileDialog::getSaveFileName(this, tr("Utiliser ce fichier"), QString());
}

void MainWindow::openGuidTXT()
{
    if(!QDesktopServices::openUrl(QUrl(ConfigReader::findParameter("guidtxt"))))
        QMessageBox::critical(this, tr("Erreur"), tr("Impossible d'ouvrir le fichier !", "GuidTXT not found"));

    return;
}

void MainWindow::openGuidWeb()
{
    if(!QDesktopServices::openUrl(QUrl(ConfigReader::findParameter("guidweb"))))
        QMessageBox::critical(this, tr("Erreur"), tr("Impossible d'accèder à la page !", "GUIDWeb inaccessible ?!"));

    return;
}

void MainWindow::openAPropos()
{
    QString email(ConfigReader::findParameter("guidemail"));
    QMessageBox::information(this, tr("A propos..."), tr("Créateur : Sopheny, Arthur POULET\n") + email);
    return;
}

void MainWindow::fileRemove(QString path)
{
    QFile file(path);
    file.remove();
    file.close();
    return;
}

quint64 MainWindow::fileSize(QString path)
{
    quint64 size;
    QFile file(path);
    size = file.size();
    file.close();
    return (size);
}

void MainWindow::generateRandomFile(QString path, quint64 size)
{
    Genkey genkey;
    genkey.setfile(path);
    genkey.setsize(size);
    genkey.generate();
    return;
}

void MainWindow::fileSafeRemove()
{
    QString file_path;
    file_path = saveFile();

    if (!file_path.isEmpty())
    {
        generateRandomFile(file_path, fileSize(file_path));
        fileRemove(file_path);
        QMessageBox::information(this, tr("Terminé"), tr("L'opération est terminée."));
    }
    return;
}

void MainWindow::testBeforeEncrypt()
{
    if (testIFile())
    {
        m_lineKFile->setEnabled(true);
        m_buttonKMode->setEnabled(true);
        m_boxIDeletion->setEnabled(true);
        m_boxIDeletion->setCheckable(true);
        execKMode();

        if (testKFile())
        {
            m_lineOFile->setEnabled(true);
            m_buttonOFile->setEnabled(true);

            if (testOFile())
            {
                m_buttonRun->setEnabled(true);
                m_statusMainbar->showMessage(tr("En attente de validation. Appuyez sur \"Lancer le programme\" pour terminer.", "attente d'appuie sur RUN"));
            }
            else
            {
                std::cout << "OFile not ready\n";
                m_buttonRun->setEnabled(false);
                m_statusMainbar->showMessage(tr("En attente de votre choix:  Choisissez un fichier pour sauvegarder le fichier une fois l'opération terminée."));
            }
        }
        else
        {
            std::cout << "KFile not ready\n";
            m_lineOFile->setEnabled(false);
            m_buttonOFile->setEnabled(false);
            m_buttonRun->setEnabled(false);
            if (m_KMode == 0)
                m_statusMainbar->showMessage(tr( \
                  "En attente de votre choix: Sélectionnez le fichier contenant votre clef"));
            else
                m_statusMainbar->showMessage(tr( \
                  "En attente de votre choix: Entrez votre mot de passe"));
        }
    }
    else
    {
        std::cout << "IFile not ready\n";
        m_buttonOFile->setEnabled(false);
        m_lineOFile->setEnabled(false);
        m_lineKFile->setEnabled(false);
        m_boxIDeletion->setEnabled(false);
        m_boxIDeletion->setCheckable(false);
        m_buttonKCopy->setEnabled(false);
        m_buttonKPaste->setEnabled(false);
        m_buttonKGenerate->setEnabled(false);
        m_buttonKMode->setEnabled(false);
        m_buttonKFile->setEnabled(false);
        m_buttonRun->setEnabled(false);
        m_statusMainbar->showMessage(tr(\
          "En attente de votre choix: Sélectionnez le fichier chiffré ou à chiffrer"));
    }
    return;
}

void MainWindow::encrypt()
{
    Cryxor encrypter(m_KMode, m_lineIFile->text(), m_lineKFile->text(), m_lineOFile->text());
    m_statusMainbar->showMessage(tr("Processus en cours..."));
    encrypter.encryptS();
    if (m_boxIDeletion->isChecked())
    {
        QFile tmp_ifile_remove(m_lineIFile->text());
        tmp_ifile_remove.remove();
        m_lineIFile->setText("");
    }
    QMessageBox::information(this, tr("Succès"), tr("L'opération est terminée. Les données Chiffrées/Déchiffrées ont été ajoutées à la fin du fichier de sauvegarde (qui a été créé s'il n'existait pas)"));
    m_statusMainbar->showMessage(tr("Terminé."));
    return;
}

void MainWindow::generateNewKey()
{
    m_lineKFile->setText(saveFile());
    if (!m_lineKFile->text().isEmpty())
    {
        m_statusMainbar->showMessage(tr("Génération d'une clé aléatoire..."));
        generateRandomFile(m_lineKFile->text(), fileSize(m_lineIFile->text()));
        QMessageBox::information(this, tr("Succès"), tr("La clef est générée."));
        testBeforeEncrypt();
    }
    return;
}

bool MainWindow::testIFile()
{
    QFileInfo file_info(m_lineIFile->text());

    if (file_info.isDir())
        return (false);
    else if (QFile(m_lineIFile->text()).exists() && !m_lineIFile->text().isEmpty())
        return (true);
    return (false);
}

bool MainWindow::testKFile()
{
    QFileInfo file_info(m_lineKFile->text());

    if (file_info.isDir())
        return (false);
    else if (!m_lineKFile->text().isEmpty())
    {
        if ((QFile(m_lineKFile->text()).exists()) || m_KMode)
            return (true);
    }
    return (false);
}

bool MainWindow::testOFile()
{
    if (m_lineOFile->text().size() < 1)
        return (false);
    else if (m_lineOFile->text().right(1) == "/")
        return (false);
    else if (m_lineOFile->text().right(1) == "\\")
        return (false);
    else if (!m_lineOFile->text().isEmpty())
        return (true);
    return (false);
}

