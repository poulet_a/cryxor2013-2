/*
CRYXOR 2013 EST SOUS LICENCE GNU GPL
Programme de chiffrement et de dechiffrement symetrique -Masque Jetable-
Sopheny (Cassiopeeu) 2013
Le programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifi
er au titre des clauses de la Licence Publique Générale GNU, telle que publiée p
ar la Free Software Foundation ; soit la version 2 de la Licence, ou (à votre di
scrétion) une version ultérieure quelconque. Ce programme est distribué dans l'e
spoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie impli
cite de COMMERCIABILITE ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la
icence Publique Générale GNU pour plus de détails. Vous devriez avoir reçu un ex
emplaire de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas
le cas, écrivez à la Free Software Foundation Inc.,  51 Franklin Street, Fifth F
loor, Boston, MA 02110-1301, USA.
*/

#ifndef MYPASSWORD_WINDOWS_HPP
#define MYPASSWORD_WINDOWS_HPP
#define MY_PASSWORD_PATH        "/.cryxor2013/mypassword.mpcx"

#include <QCoreApplication>
#include <QGuiApplication>
#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QString>
#include <QCheckBox>
#include <QLayout>
#include <QListView>
#include <QDir>
#include <QInputDialog>
#include <QMessageBox>
#include <QAction>
#include <QModelIndex>
#include <QStringListModel>

#include "crypt/cryxor.hpp"
#include "crypt/genkey.hpp"

class MyPassword_Windows : public QWidget
{
    Q_OBJECT

    public:
        MyPassword_Windows();
        int write_entry(QString url, QString pseudo, QString email, QString password, QString q1, QString q2, QString q3);
        int remove_entry(quint64 id);

    public slots:
        int initialize();
        int manage_lock(bool popup=true);
        void get_password();
        bool valid_password();
        void add_password();
        void del_password();
        void chg_password();
        void dsp_password();
        int save_password();
        void reinit_password();
        void quit();
        void update_passwordslist_control();
        void check_password_buttons(QModelIndex index);

    private:
        void initializeAllWidgets();
        void initializeAllLayouts();
        void initializeAllSignals();
        void remove_old_password();

    private:
        Cryxor m_cryxor;
        // MAIN WIDGET
        QVBoxLayout *m_layoutMain;

        // MAIN
        QVBoxLayout *m_layoutLock;
        QVBoxLayout *m_layoutContent_view;
        QHBoxLayout *m_layoutContent_buttons;

        QPushButton *m_buttonLock;
        QPushButton *m_buttonQuit;
        QPushButton *m_buttonReinit;

        QPushButton *m_buttonSavePassword;
        QPushButton *m_buttonAddPassword;
        QPushButton *m_buttonDelPassword;
        QPushButton *m_buttonChgPassword;
        QPushButton *m_buttonDspPassword;

        QString m_passwords_firstline;
        QString m_password;
        QStringList m_passwordslist;
        QStringList m_passwordslist_control;
        QStringListModel *m_passwordslist_model;
        QListView *m_passwordslist_view;
        bool m_locked;
        QString m_passwordpath;
};

#endif // MYPASSWORD_WINDOWS_HPP
