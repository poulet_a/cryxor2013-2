/*
CRYXOR 2013 EST SOUS LICENCE GNU GPL
Programme de chiffrement et de dechiffrement symetrique -Masque Jetable-
Sopheny (Cassiopeeu) 2013
Le programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifi
er au titre des clauses de la Licence Publique Générale GNU, telle que publiée p
ar la Free Software Foundation ; soit la version 2 de la Licence, ou (à votre di
scrétion) une version ultérieure quelconque. Ce programme est distribué dans l'e
spoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie impli
cite de COMMERCIABILITE ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la
icence Publique Générale GNU pour plus de détails. Vous devriez avoir reçu un ex
emplaire de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas
le cas, écrivez à la Free Software Foundation Inc.,  51 Franklin Street, Fifth F
loor, Boston, MA 02110-1301, USA.
*/

#include "genkey.hpp"
#include <iostream>

Genkey::Genkey()
{
}

void Genkey::setfile(QString path)
{
    m_path_file = path;
    return;
}

void Genkey::setsize(quint64 size)
{
    m_size = size;
    return;
}

void Genkey::generate()
{
    if (m_size > 0 && !m_path_file.isEmpty())
    {
        QFile file(m_path_file);
        file.remove();
        file.open(QIODevice::WriteOnly | QIODevice::Append);
        QDataStream sfile(&file);
        // TODO : Optimiser la boucle (generation de plus de its d'un coup pour
        //        reduire la taille de la boucle
        for(quint64 i=0; i<m_size; i++)
        {
            sfile << quint8(qrand());
        }
    }
    else
    {
        std::cout << "ERROR ! BAD PARAMETER.\n";
    }
    return;
}
