/*
CRYXOR 2013 EST SOUS LICENCE GNU GPL
Programme de chiffrement et de dechiffrement symetrique -Masque Jetable-
Sopheny (Cassiopeeu) 2013
Le programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifi
er au titre des clauses de la Licence Publique Générale GNU, telle que publiée p
ar la Free Software Foundation ; soit la version 2 de la Licence, ou (à votre di
scrétion) une version ultérieure quelconque. Ce programme est distribué dans l'e
spoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie impli
cite de COMMERCIABILITE ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la
icence Publique Générale GNU pour plus de détails. Vous devriez avoir reçu un ex
emplaire de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas
le cas, écrivez à la Free Software Foundation Inc.,  51 Franklin Street, Fifth F
loor, Boston, MA 02110-1301, USA.
*/

#include "cryxor.hpp"

using namespace std;

Cryxor::Cryxor(int mode=0, QString ipath="", QString kpath="", QString opath="")
{
    m_mode = mode;
    m_path_ifile = ipath;
    m_path_kfile = kpath;
    m_path_ofile = opath;
    return;
}
Cryxor::Cryxor(int mode)
{
    m_mode = mode;
    return;
}
Cryxor::Cryxor(void)
{
    return;
}
void Cryxor::setifile(QString path)
{
    m_path_ifile = path;
    return;
}

void Cryxor::setkfile(QString path)
{
    m_path_kfile = path;
    return;
}

void Cryxor::setofile(QString path)
{
    m_path_ofile = path;
    return;
}

QString Cryxor::getifile()
{
    return (m_path_ifile);
}

QString Cryxor::getkfile()
{
    return (m_path_kfile);
}

QString Cryxor::getofile()
{
    return (m_path_ofile);
}

void Cryxor::setmode(int mode)
{
    m_mode = mode;
    return;
}

long long Cryxor::fileSize(char* path)
{
    ifstream tmp(path, std::ios::in | std::ios::binary);
    tmp.seekg(0, std::ios::end);
    long long buff = tmp.tellg();
    tmp.close();
    return (buff);
}

void Cryxor::encryptS()
{
    if (!m_path_ifile.isEmpty() && !m_path_kfile.isEmpty() && !m_path_ofile.isEmpty())
    {
        if (m_mode==0)
        {
            long long ISize = fileSize(m_path_ifile.toUtf8().data());
            long long KSize = fileSize(m_path_kfile.toUtf8().data());
            char ci(0),ck(0),co(0);
            srand(time(NULL));
            std::ifstream tmpifile(m_path_ifile.toUtf8().data(),\
                                   std::ios::binary);
            tmpifile.seekg(0,std::ios::beg);
            std::ifstream tmpkfile(m_path_kfile.toUtf8().data(),\
                                   std::ios::binary);
            tmpkfile.seekg(0,std::ios::beg);
            std::ofstream tmpofile(m_path_ofile.toUtf8().data(),\
                                   std::ios::binary);
            tmpofile.seekp(0,std::ios::beg);

            for (int i=0; i < ISize; i++)
            {
                if (i%KSize <= KSize)
                {
                    tmpifile.get(ci);
                    tmpkfile.get(ck);
                    co=ci^ck;
                    tmpofile << co;
                }
                else
                {
                    tmpkfile.seekg(0,std::ios::beg);
                }
            }
            tmpifile.close();
            tmpkfile.close();
            tmpofile.close();
        }
        else
        {
            encryptQ();
        }
    }
    else
    {
        std::cout << "ERROR ! PARAMETER EMPTY.\n";
    }
    return;
}

void Cryxor::encryptQ()
{
    // Pas de verification des paramètres necessaires. Apprends à coder.
    quint8 i;
    quint8 k;

    // Tout par fichier
    if (m_mode == 0)
    {
        QFile ifile(m_path_ifile);
        QFile kfile(m_path_kfile);
        QFile ofile(m_path_ofile);
        ifile.open(QIODevice::ReadOnly);
        kfile.open(QIODevice::ReadOnly);
        ofile.open(QIODevice::WriteOnly | QIODevice::Append);
        QDataStream iflux(&ifile);
        QDataStream kflux(&kfile);
        QDataStream oflux(&ofile);

        while (!iflux.atEnd())
        {
            if (kflux.atEnd())
                kfile.seek(0);
            iflux >> i;
            kflux >> k;
            oflux << quint8(i^k);
        }
    }
    // Tout par fichier sauf la clée, manuelle
    else if (m_mode == 1)
    {
        QFile ifile(m_path_ifile);
        QFile ofile(m_path_ofile);
        ifile.open(QIODevice::ReadOnly);
        ofile.open(QIODevice::WriteOnly | QIODevice::Append);
        QDataStream iflux(&ifile);
        QDataStream oflux(&ofile);

        quint64 compteur;
        quint64 ksize;
        compteur = 0;
        ksize = m_path_kfile.size();

        while (!iflux.atEnd())
        {
            iflux >> i;
            k = (m_path_kfile.toUtf8()).at(compteur%ksize);
            oflux << quint8(i^k);
            compteur++;
        }
    }
    //Entrée ifile par fichier, Clef et Sortie Manuelles
    else if (m_mode == 3)
    {
        QFile ifile(m_path_ifile);
        ifile.open(QIODevice::ReadOnly);
        QDataStream iflux(&ifile);

        m_path_ofile = ""; // ON UTILISE CETTE VARIABLE POUR SORTIR LES DONNES
        quint64 compteur;
        quint64 ksize;
        compteur = 0;
        ksize = m_path_kfile.size();

        while (!iflux.atEnd())
        {
            iflux >> i;
            k = (m_path_kfile.toUtf8()).at(compteur%ksize);
            m_path_ofile.append(quint8(i^k));
            compteur++;
        }
    }
    // Entrées manuelles, sortie dans un fichier
    else if (m_mode == 4)
    {
        std::cout << m_path_ofile.toUtf8().data();
        QFile ofile(m_path_ofile);
        ofile.open(QIODevice::WriteOnly | QIODevice::Append);
        QDataStream oflux(&ofile);

        quint64 compteur;
        quint64 ksize;
        compteur = 0;
        ksize = m_path_kfile.size();

        while (compteur < (unsigned long long)(m_path_ifile.size()))
        {
            i = m_path_ifile.toUtf8().at(compteur);
            k = (m_path_kfile.toUtf8()).at(compteur%ksize);
            oflux << quint8(i^k);
            compteur++;
        }
    }
    // Entrées et sorties manuelles
    else if (m_mode == 5)
    {
        m_path_ofile = ""; // ON UTILISE CETTE VARIABLE POUR SORTIR LES DONNES
        quint64 compteur;
        quint64 ksize;
        compteur = 0;
        ksize = m_path_kfile.size();

        while (compteur < (unsigned long long)(m_path_ifile.size()))
        {
            i = m_path_ifile.toUtf8().at(compteur);
            k = (m_path_kfile.toUtf8()).at(compteur%ksize);
            m_path_ofile.append(quint8(i^k));
            compteur++;
        }
    }
    else
    {
        std::cout << "ERROR ! PARAMETER EMPTY.\n";
    }
    return;
}
