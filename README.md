Cryxor2013
==============
- Compiling (how to make)
- Compatibility (os)
- Using (how to use)
- Collaborators (who did it)
- Licence (gpl)
- Informations (site,...)



Compiling
--------------
- 1) download all source

clone the sources

    git clone https://github.com/Sophen/cryxor2013-2

- 2) install qt5 
    
With pacman, on Archlinux (use the counterpart with ubuntu etc.)

    pacman -S qt

or download qt on  http://qt-project.org/downloads, ...

- 3) Go to cryxor directory

With windows or linux etc.

    cd cryxor2013-2

- 4) make 

on linux

	qmake
	make -f Makefile.linux

or on windows

	qmake
    make -f Makefile.windows

- 5) Enjoy and stop whining ! :) 

If you have any problem, report it. You may use QtCreator.

- 1) open cryxor2013-2.pro with QtCreator.
- 2) compile, and enjoy.



Compatibility
--------------

** Linux , Windows , MacOS **
** On Linux, you have to have qt5-bases **
  When you have it, you just need to compile the source [Compiling]

** On Windows you have to have many DLL **
DLL :
- D3DCompiler_43.dll
- libgcc_s_sjlj-1.dll
- libstdc++-6.dll
- libwinpthread-1.dll
- icudt49.dll
- icuin49.dll
- icuuc49.dll
- libEGL.dll
- libGLESv2.dll
- Qt5Core.dll
- Qt5Gui.dll
- Qt5Widgets.dll
- msvcp100.dll
- platforms/qwindows.dll
- platforms/qminimal.dll

**How to get it ?**
- 1) Make a new directory (named as you want, "cryxor" for exemple)
- 2) Extract cryxor2013-2-Windows-DLL-NO-EXE.zip in this directory.
- 3) Download cryxor2013_2_X_X.zip, the last version with .conf and .exe, and extract all in the same directory.
- 4) You can run cryxor2013_2.exe.
  Then, when you have it, you just need to get cryxor2013_2.exe, via https://github.com/Sophen/cryxor2013-2/wiki/Download

**You can compile the source too with Qt (free, LGPL).**



Using
--------------
* MyPassword *
With MyPassword, you will safeguard security your bigs passwords !
Report any bug or idea to poulet.arthur@gmail.com

* BigData *
No developped

* Cryxor *
The Main program, can encrypt all datas.


You may start Cryxor2013 in 5 modes :
- GUI : juste run cryxor2013 and follow the steps
- Console : use 3 or 4 arguments 
	  1 : ifile : the file wich you have to encrypt
	  2 : kfile/key : the file wich contain the key or the key
	  3 : ofile : the file wich will contain the encrypted mail/file
	  4 : ( optionnal ) kmode : the mode that will be used to encrypt 
	      0 = basic : all streams are files
	      1 = keypass : all is files but key is a passphrase
	      2 = nop
	      3 = just the source is a file, key and outdata are strings 
	      4 = outdata is files, the rest is two strings
	      5 = all is string, no files needed



Collaborators
--------------

 - Sophen(y)



Licence
--------------
CRYXOR 2013 EST SOUS LICENCE GNU GPL .
Programme de chiffrement et de dechiffrement symetrique -Masque Jetable- .
Sopheny (Cassiopeeu) 2013 .
Le programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier au titre des clauses de la Licence Publique Générale GNU, telle que publiée par la Free Software Foundation ; soit la version 2 de la Licence, ou (à votre discrétion) une version ultérieure quelconque. Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même une garantie implicite de COMMERCIABILITE ou DE CONFORMITE A UNE UTILISATION PARTICULIERE. Voir la licence Publique Générale GNU pour plus de détails. Vous devriez avoir reçu un exemplaire de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le cas, écrivez à la Free Software Foundation Inc.,  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.



Informations
--------------
Web Site : http://sopheny.wordpress.com/cryxor2013/ (EN)

